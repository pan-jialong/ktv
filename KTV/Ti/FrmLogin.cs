﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ti
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        
        DBHelper db = new DBHelper();

        //点击取消
        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要退出吗？", Transmit.Ts, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes) System.Environment.Exit(0);
        }

        //点击登录
        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckInput())
            {
                if (GetLogin() == 1)
                {
                    MessageBox.Show("登录成功！", Transmit.Ts, MessageBoxButtons.OK);

                    //隐藏当前窗体
                    Hide();

                    //进入KTV后台管理窗体
                    FrmAdmin f = new FrmAdmin();
                    f.ShowDialog();
                    
                }
                else MessageBox.Show("登录失败！", Transmit.Ts, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //判断是否为空
        bool CheckInput()
        {
            if (textBox1.Text.Trim().Equals(string.Empty))
            {
                MessageBox.Show("请输入用户名！", Transmit.Ts, MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox1.Focus();
            }
            else if (textBox2.Text.Trim().Equals(string.Empty))
            {
                MessageBox.Show("请输入密码！", Transmit.Ts, MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox2.Focus();
            }
            else return true;
            return false;
        }

        //执行查询登录方法
        int GetLogin()
        {
            return (int)new SqlCommand(string.Format(@"select count(*) from admin_info where admin_name = {0} and 
            admin_pwd = '{1}'", textBox1.Text, textBox2.Text), db.sql).ExecuteScalar();
        }
    }
}
