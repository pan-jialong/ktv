﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TI
{
    public partial class FrmOrderBySinger : Form
    {
        public FrmOrderBySinger()
        {
            InitializeComponent();
        }

        DBHelper db = new DBHelper();
        string sex = ""; //存放是什么类型
        private void FrmOrderBySinger_Load(object sender, EventArgs e)
        {

            panel1.Dock = DockStyle.Fill;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //panel2.Visible = false;
        }

        //点击组合、男歌手、女歌手时
        private void listView1_Click(object sender, EventArgs e)
        {

            panel2.Visible = true;
            //panel2.Visible = false;
            panel1.Visible = false;
            //panel2.Location = panel1.Location;
            panel2.Dock = DockStyle.Fill;
            GetArea();

        }

        //点击大陆、香港、台湾地区时
        private void listView2_Click(object sender, EventArgs e)
        {

            panel3.Visible = true;
            panel2.Visible = false;
            //panel3.Location = panel2.Location;
            panel3.Dock = DockStyle.Fill;
            Get();
        }

        //换面板方法
        void GetArea()
        {
            switch (listView1.SelectedIndices[0])
            {
                case 0:
                    sex = "组合";
                    listView2.LargeImageList = imagGroup;
                    break;
                case 1:
                    sex = "男";
                    listView2.LargeImageList = imageList1;
                    break;
                case 2:
                    sex = "女";
                    listView2.LargeImageList = imagGril;
                    break;
            }
        }

        //切换歌手方法
        void Get()
        {
            if (listView3.Items != null) listView3.Items.Clear();
            switch (listView1.SelectedIndices[0])
            {
                case 0:
                    listView3.LargeImageList = imagGroup;
                    break;
                case 1:
                    listView3.LargeImageList = imageList1;
                    break;
                case 2:
                    listView3.LargeImageList = imagGril;
                    break;

            }
            SqlDataReader reader = new SqlCommand(string.Format("select singer_name from singer_info where singertype_id = {0} and singer_sex = '{1}'",
                listView2.SelectedIndices[0] + 1, sex), db.sql).ExecuteReader();
            int num = 0;
            while (reader.Read())
            {
                listView3.Items.Add(reader[0] + "", num++);

            }
            reader.Close();
        }

        DataSet ds = new DataSet();
        SqlDataAdapter sd;


        //显示歌手歌曲
        void GetSong()
        {
            //判断是否选中一行
            if (listView3.SelectedItems.Count > 0)
            {
                
                sd = new SqlDataAdapter(string.Format(@"select si.song_name,si.song_play_count from singer_info s, song_info si where s.[singer_id] = 
                si.singer_id and s.singer_name = '{0}' ", listView3.SelectedItems[0].Text), db.sql);


                //判断是否已存在，存在就删除
                if (ds.Tables["song_info"] != null) ds.Tables["song_info"].Clear();

                //再重新添加进来
                sd.Fill(ds, "song_info");

                //声明一个动态视图
                DataView dv = ds.Tables["song_info"].DefaultView;

                //绑定数据源
                dataGridView1.DataSource = dv;
            }
        }

        //点击返回
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            if (panel1.Visible) Close();
            else if (panel2.Visible)
            {
                panel1.Visible = true;
                panel2.Visible = false;
            }
            else if (panel3.Visible)
            {
                panel2.Visible = true;
                panel3.Visible = false;
            }
            else if (panel4.Visible)
            {
                panel3.Visible = true;
                panel4.Visible = false;
            }

        }

        //点击面板3
        private void listView3_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel3.Visible = false;
            panel4.Dock = DockStyle.Fill;
            panel4.Visible = true;
            GetSong();
            //静止选择行
            dataGridView1.ClearSelection();
        }

        //点击主窗体
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Close();
        }

        //双击歌曲时触发歌曲播放
        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SqlDataReader reader = new SqlCommand(string.Format(@"select si.song_name ,si.song_play_count,si.[song_url]
            from singer_info s, song_info si where s.[singer_id] = si.singer_id and s.singer_name = '{0}'",
            listView3.SelectedItems[0].Text), db.sql).ExecuteReader();
            reader.Read();
            FrmPlay.geQu.Add(reader[2] + "");
            reader.Close();

            //FrmPlay f = new FrmPlay();
            //f.Show();
            //f.Hide();
        }
    }
}
