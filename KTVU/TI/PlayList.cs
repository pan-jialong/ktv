﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI
{
    class PlayList
    {
        //歌曲播放列表数组
        private static Song[] songList = new Song[50];
        //歌曲播放列表动态数组
        private static List<Song> songs = new List<Song>();

        //当前播放的歌曲在数组中的索引
        private static int songIndex;

        //当前播放的歌曲名称
        public string PlayingSongName()
        {
            return songs[songIndex].SongName1;
        }

        //获取当前播放的歌曲
        public string GetPlayingSong()
        {
            return songs[songIndex].SomgURL;
        }

        //获取下一首要播放的歌曲名称
        public string NextSongName()
        {
            return songs[songIndex + 1].SongName1;
        }

        //点播一首歌曲
        public void AddSong(Song song) { }

        //切歌
        public void CutSong(int index) { }

        //重放当前播放的歌曲
        public void PlayAgain() { }

        //播放下一首歌曲
        public void MoveOn() { }


        //添加歌曲
        public void Add(string name, string url)
        {
            songs.Add(new Song
            {
                SongName1 = name
                ,
                SomgURL = url
            }) ;
            songIndex++;
        }
    }
}
