﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient; 

namespace TI
{
    public partial class FrmOrderBySongType : Form
    {
        public FrmOrderBySongType()
        {
            InitializeComponent();
        }


        DBHelper db = new DBHelper();

        //进入窗体触发
        private void FrmOrderBySongType_Load(object sender, EventArgs e)
        {
            SqlDataReader reader = new SqlCommand("select songtype_name from song_type", db.sql).ExecuteReader();
            listView1.LargeImageList = imageList1;
            int num = 0;
            while (reader.Read())
            {
                listView1.Items.Add(reader[0] + "", num++);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        //点击返回
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
