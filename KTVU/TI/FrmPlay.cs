﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace TI
{
    public partial class FrmPlay : Form
    {
        public FrmPlay()
        {
            InitializeComponent();
        }

        static List<Song> songs = new List<Song>();

        public static List<string> geQu = new List<string>();

        //string[] fileName;
        int index = 0;
        private void FrmPlay_Load(object sender, EventArgs e)
        {

            //axWindowsMediaPlayer1.URL = @"D:\网易云\周玥 - 夜空中最亮的星.mp3"; //路径

            //axWindowsMediaPlayer1.Ctlcontrols.stop();  //停止播放
            axWindowsMediaPlayer1.enableContextMenu = false;  //启用/禁用右键菜单
            axWindowsMediaPlayer1.settings.autoStart = true;  //是否自动播放

            //对话框，选择文件的
            //OpenFileDialog ofd = new OpenFileDialog();
            //ofd.Title = "请选择音乐";
            //ofd.InitialDirectory = @"D:\网易云";
            //ofd.Filter = "(*.mp3)|*.mp3";
            //ofd.Multiselect = true;
            //if (ofd.ShowDialog() == DialogResult.OK)
            //{
            //      fileName = ofd.FileNames;  //获取路径
            //}

            IWMPMedia media;

            foreach (string s in geQu)
            {
                //MessageBox.Show(s.Substring(s.LastIndexOf('\\') + 1));
                //media = axWindowsMediaPlayer1.newMedia(s.Substring(s.LastIndexOf('\\') + 1));
                media = axWindowsMediaPlayer1.newMedia(s);
                axWindowsMediaPlayer1.currentPlaylist.appendItem(media);

                //绑定数据下多行文本上
                listBox1.Items.Add(s.Substring(s.LastIndexOf('\\') + 1));
            }
            axWindowsMediaPlayer1.Ctlcontrols.play();

            //选择第一项
            //listBox1.SelectedIndex = 0;
            //axWindowsMediaPlayer1.URL = fileName[listBox1.SelectedIndex];

            //axWindowsMediaPlayer1.Visible = false;//隐藏播放器
            
        }

        //private void axWindowsMediaPlayer1_Enter(object sender, EventArgs e)
        //{
        //    //MessageBox.Show(axWindowsMediaPlayer1.currentMedia.sourceURL);//返回当前路径
        //    //MessageBox.Show(axWindowsMediaPlayer1.currentMedia.name);          //获取正在播放的媒体文件的名称
        //}


        //状态改变事件
        private void axWindowsMediaPlayer1_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (axWindowsMediaPlayer1.playState == WMPLib.WMPPlayState.wmppsReady)
            {
                try
                {
                    axWindowsMediaPlayer1.Ctlcontrols.next();  //下一首歌
                }
                catch (Exception a)
                {
                    MessageBox.Show(a.ToString());
                }
            }

        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            //axWindowsMediaPlayer1.Ctlcontrols.stop();

            //axWindowsMediaPlayer1.URL = fileName[++index];
            //listBox1.SelectedIndex = index;
            //axWindowsMediaPlayer1.Ctlcontrols.play();
            //btnPlay.Text = "暂停";
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void bb_Click(object sender, EventArgs e)
        {
            //axWindowsMediaPlayer1.Ctlcontrols.stop();

            //axWindowsMediaPlayer1.URL = fileName[--index];
            //listBox1.SelectedIndex = index;
            //axWindowsMediaPlayer1.Ctlcontrols.play();
        }
    }
}
