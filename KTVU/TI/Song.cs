﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TI
{
    class Song
    {

        private string songName;    //歌曲名称

        private string somgURL;     //歌曲存放路径

        private SongPlayState playState = SongPlayState.unplayer;  //歌曲播放状态

        internal SongPlayState PlayState;    //歌曲播放状态属性

        //歌曲名称
        public string SongName1 { get => songName; set => songName = value; }

        //歌曲存放路径属性
        public string SomgURL { get => somgURL; set => somgURL = value; }
    }

    //枚举：歌曲播放状态
    public enum SongPlayState
    {
        unplayer, //未播放
        played,   //已播放
        again,    //重唱
        cut       //切歌，
    }
}
