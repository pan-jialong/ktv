﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TI
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            FrmPlay f = new FrmPlay();
            f.Show();
            f.Hide();
        }

        //点击歌星点歌
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            FrmOrderBySinger fo = new FrmOrderBySinger();
            fo.ShowDialog();
        }

        //点击拼音点歌
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            FrmOrderBySongName n = new FrmOrderBySongName();
            n.ShowDialog();
        }

        //点击分类点歌
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            FrmOrderBySongType n = new FrmOrderBySongType();
            n.ShowDialog();
        }

        //点击金榜排名
        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        //点击数字点歌
        private void pictureBox5_Click(object sender, EventArgs e)
        {
            FrmOrderByWordCount n = new FrmOrderByWordCount();
            n.ShowDialog();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        //点击退出
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要退出吗", Tool.TS, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                 == DialogResult.Yes) System.Environment.Exit(0); 
        }
    }
}
